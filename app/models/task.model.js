
// */app/models/todo.model.js*

// ## Todo Model

// Note: MongoDB will autogenerate an _id for each Todo object created

// Grab the Mongoose module
var mongoose = require('mongoose');

// Create a `schema` for the `Task` object
    var taskSchema = new mongoose.Schema({
        title: { type : String },
        isDone: {type: Boolean}
    });

// Expose the model so that it can be imported and used in
// the controller (to search, delete, etc.)
//export default mongoose.model('Task', taskSchema);

module.exports = mongoose.model('Task', taskSchema);
    
    

