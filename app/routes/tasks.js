
var mongojs = require('mongojs');

var Task = require('../models/task.model');
module.exports = function (app, router) {
    router.get('/tasks', function (req, res, next) {
        Task.find((err, tasks) => {
            if (err)
                res.send(err);

            res.json(tasks);
        });
    });

// Get Single Task
    router.get('/task/:id', function (req, res, next) {
        Task.findOne({_id: mongojs.ObjectId(req.params.id)}, function (err, task) {
            if (err) {
                res.send(err);
            }
            res.json(task);
        });
    });

//Save Task
    router.post('/task', function (req, res, next) {
        var task = req.body;
        if (!task.title || !(task.isDone + '')) {
            res.status(400);
            res.json({
                "error": "Bad Data"
            });
        } else {
            Task.create(task, function (err, task) {
                if (err) {
                    res.send(err);
                }
                res.json(task);
            });

        }
    });

// Delete Task
    router.delete('/task/:id', function (req, res, next) {
        Task.remove({_id: mongojs.ObjectId(req.params.id)}, function (err, task) {
            if (err) {
                res.send(err);
            }
            res.json(task);
        });
    });

// Update Task
    router.put('/task/:id', function (req, res, next) {
        var task = req.body;
        var updTask = {};

        if (task.isDone) {
            updTask.isDone = task.isDone;
        }

        if (task.title) {
            updTask.title = task.title;
        }

        if (!updTask) {
            res.status(400);
            res.json({
                "error": "Bad Data"
            });
        } else {
            Task.update({_id: mongojs.ObjectId(req.params.id)}, updTask, {}, function (err, task) {
                if (err) {
                    res.send(err);
                }
                res.json(task);
            });
        }
    });

};