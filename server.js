
// # Node Env Variables

// Load Node environment variable configuration file
var validateEnvVariables = require('./config/env.conf.js');

// Set up appropriate environment variables if necessary
validateEnvVariables();

var express = require('express');
var path = require('path');
var port = 3000;
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
// Load Mongoose config file for connecting to MongoDB instance
var mongooseConf = require('./config/mongoose.conf.js');
// Pass Mongoose configuration Mongoose instance
mongooseConf(mongoose);
var passport = require('passport');
require('./config/passport')(passport); // pass passport for configuration
var flash    = require('connect-flash');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var session      = require('express-session');
//var configDB = require('./config/mongoose.conf.js');
// Get an instance of the express Router
var router = express.Router();

// configuration ===============================================================
//mongoose.connect(mongooseConf); // connect to our database

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
// Body Parser MW
app.use(bodyParser.json());// get information from html forms
app.use(bodyParser.urlencoded({extended: false}));

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
// routes ======================================================================
require('./app/routes.js')(app, router, passport); // load our routes and pass in our app and fully configured passport


app.listen(port, function(){
    console.log('Server started on port '+port);
});